/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */

#include "terasic_includes.h"
#include "ILI9341.h"
#include "gui.h"
#include "touch_spi.h"
#include "system.h"

void GUI_VPG(alt_video_display *pDisplay, TOUCH_HANDLE *pTouch);

int main()
{
	TOUCH_HANDLE *pTouch;
	alt_video_display Display;


    const bool bVPG = ((IORD(KEY_BASE, 0x00) & 0x01) == 0x00)?TRUE:FALSE;

   printf("LT24 Demo Test!\n");

   // init touch
   pTouch = Touch_Init(TOUCH_PANEL_SPI_BASE, TOUCH_PANEL_PEN_IRQ_N_BASE, TOUCH_PANEL_PEN_IRQ_N_IRQ);
   if (!pTouch){
       printf("Failed to init touch\r\n");
   }else{
       printf("Init touch successfully\r\n");

   }

   // init LCD
   LCD_Init();
   LCD_Clear(0X0000);

   Display.interlace = 0;
   Display.bytes_per_pixel = 2;
   Display.color_depth = 16;
   Display.height = SCREEN_HEIGHT;
   Display.width = SCREEN_WIDTH;
//
//   // run demo
   /*
  if (bVPG)
       GUI_VPG(&Display, pTouch); // enter vpg mode when users press KEY0
   else
	   GUI(&Display, pTouch);

*/
   GUI_ShowTrafficLight(&Display, 40, 80, 40, 160 );
}
