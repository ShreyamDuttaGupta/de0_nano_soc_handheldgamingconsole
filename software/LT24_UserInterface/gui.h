#ifndef GUI_H_
#define GUI_H_

#include "alt_video_display.h"
#include "touch_spi.h"

void GUI(alt_video_display *pDisplay, TOUCH_HANDLE *pTouch);
void GUI_ShowTrafficLight(alt_video_display *pDisplay, int xstart, int xend, int ystart, int yend);

#endif /*GUI_H_*/
